## A JXON Library for Demandware

---

This is a library for converting E4X objects into Javascript and vice-versa.   

This library is inspired by the Mozilla JXON code found at https://developer.mozilla.org/en-US/docs/JXON.

The code includes a simple testing framework I put together in order to test JXON.

Enjoy!

### Example: Convert E4X object into JS object.

```
	var JXON = require("./JXON");
	...
	var obj = JXON.toJS(<animal name="Deka">
							is my cat
						</animal>);
	
	Assert.isObject(obj, "obj");			
	Assert.isObject(obj.animal, "obj.animal");
	Assert.areEqual("Deka", obj.animal["@name"]);
	Assert.areEqual("is my cat", obj.animal.keyValue);
```

### Example: Convert JS object into E4X.
```
    var JXON = require("./JXON");
	...
	var xml = JXON.toXML({
							root: {
							  myboolean: true,
							  myarray: ["Cinema", "Hot dogs", false],
							  myobject: {
							    nickname: "Jack",
							    registration_date: new Date(1995, 11, 25),
							    privileged_user: true
							  },
							  mynumber: 99,
							  mytext: "Hello World!"
							}
						});
	
				
	Assert.isNotNull(xml);
	Assert.isTrue(xml instanceof XML, "xml not an XML object");
	Assert.areEqual("true", xml.myboolean.toString());
	Assert.areEqual(3, xml.myarray.length());
	Assert.areEqual("Jack", xml.myobject.nickname.toString());
	Assert.areEqual("Mon, 25 Dec 1995 00:00:00 GMT", xml.myobject.registration_date.toString());
	Assert.areEqual("true", xml.myobject.privileged_user.toString());
	Assert.areEqual("99", xml.mynumber.toString());
	Assert.areEqual("Hello World!", xml.mytext.toString());
``` 

The following is a table showing different conversion patterns:

Case | XML | JSON | Javascript Access
------------ | ------------- | ------------
1 | <animal /> |"animal": {} |	myObject.animal
2 | <animal>Deka</animal> | "animal": "Deka"| myObject.animal
3 | <animal name="Deka" /> | "animal": {"@name": "Deka"} | myObject.animal["@name"]
4 | <animal name="Deka">is my cat</animal> |	"animal": { "@name": "Deka", "keyValue": "is my cat" } | myObject.animal["@name"], myObject.animal.keyValue
5 | <animal> <dog>Charlie</dog> <cat>Deka</cat> </animal> | "animal": { "dog": "Charlie", "cat": "Deka" } | myObject.animal.dog, myObject.animal.cat
6 | <animal> <dog>Charlie</dog> <dog>Mad Max</dog> </animal> | "animal": { "dog": ["Charlie", "Mad Max"] }	| myObject.animal.dog[0], myObject.animal.dog[1]
7 | <animal> in my house <dog>Charlie</dog> </animal> | "animal": { "keyValue": "in my house", "dog": "Charlie" }	| myObject.animal.keyValue, myObject.animal.dog
8 | <animal> in my ho <dog>Charlie</dog> use </animal> | "animal": { "keyValue": "in my house", "dog": "Charlie" }	| myObject.animal.keyValue, myObject.animal.dog